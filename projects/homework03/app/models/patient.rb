class Patient < ActiveRecord::Base
	has_many :appointments
	has_many :specialists, through: :appointments
	has_one :insurance
end
