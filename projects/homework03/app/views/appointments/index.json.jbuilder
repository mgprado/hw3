json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :specialist_id, :patient_id, :appointment_date, :fee
  json.url appointment_url(appointment, format: :json)
end
