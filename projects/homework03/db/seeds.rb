Patient.delete_all
Specialist.delete_all
Insurance.delete_all
Appointment.delete_all

p1 = Patient.create!(name: 'Jack Harkness', street_address: '1234 Main Street')
p2 = Patient.create!(name: 'Alistair Gordon Lethbridge-Stewart', street_address: '1627 Boardwalk')
p3 = Patient.create!(name: 'Rose Tyler', street_address: '43689 Park Place')
p4 = Patient.create!(name: 'Amy Pond', street_address: '1234 Pennsylvania Ave')
p5 = Patient.create!(name: 'Sarah Jane Smith', street_address: '1234 North Carolina Ave')

s1 = Specialist.create!(name:'Jon Pertwee', speciality: 'Internalist')
s2 = Specialist.create!(name:'Tom Baker', speciality: 'Orthopedics')
s3 = Specialist.create!(name:'David Tennant', speciality: 'Allergist')
s4 = Specialist.create!(name:'Pauk McGann', speciality: 'Ears, nose, and throat')
s5 = Specialist.create!(name:'Christopher Eccleston', speciality: 'Neurolgy')

Insurance.create!(name: 'State Farm', street_address: 'Old Rent Road')
Insurance.create!(name: 'Texas Farmers', street_address: 'White Chapel Road')
Insurance.create!(name: 'Allstate', street_address: 'Euston Road')
Insurance.create!(name: 'Humana', street_address: 'Pentonville Road')
Insurance.create!(name: 'Liberty', street_address: 'Bow Street')

Appointment.create!(patient: p1,specialist: s1, appointment_date: '2014-10-23',fee: 275)
Appointment.create!(patient: p1,specialist: s2, appointment_date: '2014-11-13',fee: 1275)
Appointment.create!(patient: p2,specialist: s3, appointment_date: '2014-12-27',fee: 25)
Appointment.create!(patient: p3,specialist: s4, appointment_date: '2014-10-10',fee: 675)
Appointment.create!(patient: p4,specialist: s5, appointment_date: '2014-10-01',fee: 175)
Appointment.create!(patient: p3,specialist: s2, appointment_date: '2014-12-25',fee: 245)
Appointment.create!(patient: p2,specialist: s1, appointment_date: '2014-11-15',fee: 2745)